resource "random_pet" "prefix" {}

provider "azurerm" {
  # version = "~> 2.0"
  features {}
}

resource "azurerm_resource_group" "default" {
  name     = "${random_pet.prefix.id}-rg"
  location = "West Europe"

  tags = {
    environment = "Demo"
  }
}

resource "azurerm_virtual_network" "vnet" {
  name                = "${random_pet.prefix.id}-aks-vnet"
  address_space       = ["10.30.0.0/16"]
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "${random_pet.prefix.id}-aks-subnet"
  resource_group_name  = azurerm_resource_group.default.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefix       = "10.30.1.0/24"
}

module "k8s-cluster" {
  source = "./modules/k8s-cluster"

  name                = "${random_pet.prefix.id}-aks"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  dns_prefix          = "${random_pet.prefix.id}-k8s"
  vnet_subnet_id      = azurerm_subnet.subnet.id
  appId               = var.appId
  password            = var.password
}

module "deployment" {
  source = "./modules/deployment"

  deploy_name = "scalable-nginx"
  app_name = "ScalableNginx"
}

module "service" {
  source = "./modules/service"

  service_name = "nginx-service"
  app_name = "ScalableNginx"
}
