resource "kubernetes_deployment" "nginx" {

  metadata {
    name = var.deploy_name
    labels = {
      App = var.app_name
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = var.app_name
      }
    }
    template {
      metadata {
        labels = {
          App = var.app_name
        }
      }
      spec {
        container {
          image = "nginx:1.7.8"
          name  = "nginx"

          port {
            container_port = 80
          }
        }
      }
    }
  }
}
