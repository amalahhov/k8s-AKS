variable "deploy_name" {
    description = "Deployment variable"
    type = string
}

variable "app_name" {
    description = "App name"
    type = string
}
