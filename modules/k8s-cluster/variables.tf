variable "name" {
  description = "Name for cluster"
  type = string
}

variable "location" {
  description = "Location for cluster"
  type = string
}

variable "resource_group_name" {
  description = "Resource group name for cluster"
  type = string
}

variable "dns_prefix" {
  description = "DNS for cluster"
  type = string
}
variable "vnet_subnet_id" {
  description = "Subnet"
  type = string
}

variable "appId" {}
variable "password" {}
