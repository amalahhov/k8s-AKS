resource "kubernetes_service" "nginx" {
  metadata {
    name = var.service_name
  }
  spec {
    selector = {
      App = var.app_name
    }
    port {
      port        = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }
}
