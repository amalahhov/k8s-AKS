output "resource_group_name" {
  value = azurerm_resource_group.default.name
}

output "kubernetes_cluster_name" {
  value = module.k8s-cluster.kubernetes_cluster_name
}

output "host" {
  value = module.k8s-cluster.host
}

output "client_key" {
  value = module.k8s-cluster.client_key
}

output "client_certificate" {
  value = module.k8s-cluster.client_certificate
}

output "kube_config" {
  value = module.k8s-cluster.kube_config
}

output "cluster_username" {
  value = module.k8s-cluster.cluster_username
}

output "cluster_password" {
  value = module.k8s-cluster.cluster_password
}

output "cluster_IP" {
  value = module.service.lb_ip
}
