terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "1.13.3"
    }
  }
}

provider "kubernetes" {
  load_config_file = false

  host     = module.k8s-cluster.host
  username = module.k8s-cluster.cluster_username
  password = module.k8s-cluster.cluster_password

  client_certificate     = "base64decode(module.k8s-cluster.client_certificate)"
  client_key             = "base64decode(module.k8s-cluster.client_key)"
  cluster_ca_certificate = "base64decode(module.k8s-cluster.cluster_ca_certificate)"
}
